package com.ion.contact;

public class Correction {
  private double rate;
  private String correction;
  
  Correction(double rate, String correction) {
    this.rate = rate;
    this.correction = correction;
  }
  
  public double getMatchRate() {
    return rate;
  }
  
  public String getCorrectedText() {
    return this.correction;
  }
}
