package com.ion.contact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class ContactConfig {
  
  private static ArrayList<Locale> supportedLocales;
  private static HashMap<Locale, ContactDataInspector> inspectors;
  private static final String curr_package;

  static {
    curr_package = ContactDataInspector.class.getPackage().getName();
  }
  
  public static ArrayList<Locale> getSupportedLocales() {
    if (supportedLocales == null) {
      ArrayList<Locale> locales = new ArrayList<>();
  
      locales.add(Locale.KOREAN);
      // ...
      supportedLocales = locales;
    }
    return supportedLocales;
  }


  public static HashMap<Locale, ContactDataInspector> getContactDataInspectors() {
    if (inspectors == null) {
      HashMap<Locale, ContactDataInspector> map = new HashMap<>();
      try {
        for (Locale locale : getSupportedLocales()) {
          String className = curr_package + "." + locale + ".ContactDataInspectorImpl";
          Class<?> c = Class.forName(className);
          ContactDataInspector inspector = (ContactDataInspector)c.newInstance();
          map.put(locale, inspector);
        }
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
      inspectors = map;
    }
    return inspectors;
  }
  
}
