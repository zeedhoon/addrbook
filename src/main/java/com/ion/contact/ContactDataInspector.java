package com.ion.contact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.TreeMap;

import org.json.JSONObject;

import com.ion.contact.ko.OldNameScanner;
import com.ion.contact.util.LocaleDetector;
import com.ion.contact.util.LocaleInfo;

public abstract class ContactDataInspector {
  private ArrayList<ContactScanner> scanners;
  
  public ContactDataInspector(ArrayList<ContactScanner> scanners) {
    this.scanners = scanners;
  }

  public static ScanResult inspect(JSONObject addrData, Locale locale) {
//    String fullName = addrData.optString(ContactField.FULL_NAME.getKey());
//    LocaleInfo localeInfo = LocaleDetector.detectLocale(fullName);
//    ContactDataInspector inspector = getInspector(localeInfo.getPrimaryLocale());
    
    ContactDataInspector inspector = getInspector(locale);
    return inspector.inspectInternal(addrData);
  }
  
  protected ScanResult inspectInternal(JSONObject addrData) {
    ScanResult res = new ScanResult();
    for (ContactScanner scanner : scanners) {
      scanner.scan(addrData, res);
    }
    return res;
  }

  public static ContactDataInspector getInspector(Locale locale) {
    return ContactConfig.getContactDataInspectors().get(locale);
  }

}
