package com.ion.contact;


public class ContactToken {
  
  private ContactField type;
  private String token;
  private int flags;
  
  public static final int WORD_BREAK_BEFORE = 1;
  public static final int WORD_BREAK_AFTER = 2;
  
  
  public ContactToken(String token, ContactField type, int flags) {
    this.token = token;
    this.type = type;
    this.flags = flags;
  }

  public final ContactField getField() {
    return type;
  }
  
  public String toString() {
    return getValue();
  }

  public String getKey() {
    return type.getKey();
  }
  
  public String getValue() {
    return token;
  }
  
  public boolean isWordStart() {
    return (flags & WORD_BREAK_BEFORE) != 0;
  }

  public boolean isWordEnd() {
    return (flags & WORD_BREAK_AFTER) != 0;
  }
}