package com.ion.contact;

import org.json.JSONObject;

public interface ContactScanner {

  public void scan(JSONObject addrData, ScanResult scanResult);

}
