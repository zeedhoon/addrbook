package com.ion.contact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.json.JSONObject;

public class ScanResult {
  
  HashMap<ContactField, ArrayList<Correction>> suggestions = new HashMap<>(); 

  public void add(ContactField contactField, String value, double matchRate) {
    ArrayList<Correction> corretions = suggestions.get(contactField);
    if (corretions == null) {
      corretions = new ArrayList<Correction>();
      suggestions.put(contactField, corretions);
    }
    corretions.add(new Correction(matchRate, value));
    
  }

  public JSONObject getPrimarySuggestion() {
    JSONObject json = new JSONObject();
    for (Entry<ContactField, ArrayList<Correction>> entry : suggestions.entrySet()) {
      String key = entry.getKey().getKey();
      String value = entry.getValue().get(0).getCorrectedText();
      json.put(key, value);
    }
    return json;
  }

}
