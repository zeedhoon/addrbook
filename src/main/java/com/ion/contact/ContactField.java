package com.ion.contact;

public enum ContactField {
  NAME_PREFIX("namePrefix"), // "접두사"
  FULL_NAME("fullName"), // "성명"
  FAMILY_NAME("familyName"), //  "성"
  FAMILY_NAME_YOMI("familyNameYomi"), // "성(소리나는 대로)"
  ADDTIONAL_NAME("additionalName"), // "가운데이름"
  ADDTIONAL_NAME_YOMI("additionalNameYomi"), // "가운데이름(소리나는 대로)"
  GIVEN_NAME("givenName"), // "이름"
  GIVEN_NAME_YOMI("givenNameYomi"), // "이름(소리나는 대로)"
  NAME_SUFFIX("nameSuffix"), // "접미사"
  NICKNAME("nickname"), // "닉네임"
  FILE_AS("fileAs"), // "다른 이름으로 저장"
  ORG_NAME("orgName"), // "회사명"
  ORG_DEPARTMENT("orgDepartment"), // "부서"
  ORG_TITLE("orgTitle"), // "직책"
  EMAIL("email"), // "이메일"
  PHONE_NUMBER("phoneNumber"), // "전화"
  WEBSITE("website"), // "웹사이트"
  RELATION("relation"), // "결혼/연애"
  IM("im"), // "채팅"
  POSTAL_ADDRESS("structuredPostalAddress"), // "우편주소"
  CONTENT("content"), // "메모"
  EX_SEPARATOR(null); 

  private final String key;
  
  ContactField(String key) {
    this.key = key;
  }
  
  public final String getKey() {
    return this.key;
  }
  
  public String toString() {
    return this.key;
  }
}
