package com.ion.contact.util;

import java.util.ArrayList;
import java.util.Locale;

public class LocaleInfo {
  ArrayList<LocaleMatch> localeMatches = new ArrayList<>();
  
  public Locale getPrimaryLocale() {
    if (localeMatches.isEmpty()) {
      return null;
    }
    return localeMatches.get(0).locale;
  }

  public void addMatchedLocale(Locale locale) {
    int idx = localeMatches.size();
    for (; --idx >= 0; ) {
      LocaleMatch match = localeMatches.get(idx);
      if (match.locale == locale) {
        int matchCount = match.count += 1;
        int inserAfter = idx;
        for ( ; --inserAfter >= 0; ) {
          LocaleMatch prev = localeMatches.get(inserAfter);
          if (prev.count >= matchCount) {
            break;
          }
        }
        if (inserAfter < idx - 1) {
          localeMatches.remove(idx);
          localeMatches.add(inserAfter, match);
        }
        return;
      }
    }
    LocaleMatch match = new LocaleMatch();
    match.count = 1;
    match.locale = locale;
    localeMatches.add(match);
  }
  
  static class LocaleMatch {
    int count;
    Locale locale;
  }

}
