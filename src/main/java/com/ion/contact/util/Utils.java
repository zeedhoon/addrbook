package com.ion.contact.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.function.BooleanSupplier;

import org.json.JSONObject;

public class Utils {

  public static boolean isEmpty(String familyName) {
    return familyName == null || familyName.trim().length() == 0;
  }

  public static String cutWord(String text, int start) {
    
    for (int i = start; i < text.length(); i ++) {
      char ch = text.charAt(i);
      if (ch <= ' ') {
        if (i == start) {
          start = i;
        }
        else {
          return text.substring(start, i);
        }
      }
    }
    return text.substring(start).trim();      
  }

  public static String cutLastWord(String text) {
    for (int i = text.length(); --i >= 0; ) {
      char ch = text.charAt(i);
      if (ch <= ' ') {
          return text.substring(i);
      }
    }
    return "";      
  }  
  
  public static String[] loadSortedWordList(InputStream in, int min_len, int max_len) throws IOException {
    String[] array = loadWordList(in, min_len, max_len);
    Arrays.parallelSort(array);
    return array;
  }
  
  public static String[] loadWordList(InputStream in, int min_len, int max_len) throws IOException {
    StringBuilder sb = new StringBuilder();
    ArrayList<String> words = new ArrayList<>();
    
    InputStreamReader r = new InputStreamReader(in, "utf-8");
    for (int ch; (ch = r.read()) != -1; ) {
      if (ch > ' ') {
        sb.append((char)ch);
      }
      else if (sb.length() > 0) {
        if (sb.length() < min_len) {
          throw new RuntimeException("Too short word: length of '" + sb + "' < " + min_len);
        }
        if (sb.length() > max_len) {
          throw new RuntimeException("Too long word: length of '" + sb + "' > " + max_len);
        }
        words.add(sb.toString());
        sb.setLength(0);
      }
    }
    String[] data = new String[words.size()];
    words.toArray(data);
    return data;
  }

  public static char[] loadSortedCharArray(InputStream in) throws IOException {
    char[] array = loadCharArray(in);
    Arrays.parallelSort(array);
    return array;
  }
  
  public static char[] loadCharArray(InputStream in) throws IOException {
    InputStreamReader r = new InputStreamReader(in, "utf-8");
    StringBuilder sb = new StringBuilder();
    for (int ch; (ch = r.read()) != -1; ) {
      if (ch > ' ') {
        sb.append((char)ch);
      }
    }
    char[] data = new char[sb.length()];
    sb.getChars(0, sb.length(), data, 0);
    return data;
  }

  public static String binarySearchPrefix(String[] tokens, String text) {
    int idx = Arrays.binarySearch(tokens, text);
    if (idx >= 0) {
      return tokens[idx];
    }
    else {
      idx = ~idx - 1;
      if (idx >= 0) {
        String prev = tokens[idx];
        if (text.startsWith(prev)) {
          return prev;
        }
      }
    }
    return null;
  }

  public static boolean equals(JSONObject expected, JSONObject results) {
    
    if (expected.length() != results.length()) {
      return false;
    }
    
    for (String key : expected.keySet()) {
      String expected_v = expected.getString(key);
      String result_v = results.optString(key, null);
      
      if (!Objects.equals(expected_v, result_v)) {
        return false;
      }
    }
    return true;
  }

  public static String toString(InputStream in) throws IOException {
    InputStreamReader r = new InputStreamReader(in);
    StringBuilder sb = new StringBuilder();
    for (int ch; (ch = r.read()) >= 0; ) {
      sb.append((char)ch);
    }
    return sb.toString();
  }

}
