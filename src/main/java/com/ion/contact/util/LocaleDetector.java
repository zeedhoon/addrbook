package com.ion.contact.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class LocaleDetector {

  static HashMap<Character.UnicodeBlock, Locale[]> localeMap = new HashMap<>(); 
  
  static {
    addLocaleInfo(Character.UnicodeBlock.HANGUL_SYLLABLES, Locale.KOREAN);
    addLocaleInfo(Character.UnicodeBlock.HANGUL_JAMO, Locale.KOREAN);
    addLocaleInfo(Character.UnicodeBlock.HANGUL_JAMO_EXTENDED_A, Locale.KOREAN);
    addLocaleInfo(Character.UnicodeBlock.HANGUL_JAMO_EXTENDED_B, Locale.KOREAN);
    addLocaleInfo(Character.UnicodeBlock.HANGUL_COMPATIBILITY_JAMO, Locale.KOREAN);
  }
  
  private static void addLocaleInfo(Character.UnicodeBlock codeBlock, Locale... locales) {
    localeMap.put(codeBlock, locales);
  }
  

  public static LocaleInfo detectLocale(String text) {
    int code_len = text.codePointCount(0, text.length());
    LocaleInfo localeInfo = new LocaleInfo();
    for (int i = 0; i < code_len; i ++) {
      Character.UnicodeBlock cb = Character.UnicodeBlock.of(text.codePointAt(0));
      Locale[] codeLocales = localeMap.get(cb);
      if (codeLocales == null) continue;
      
      for (Locale locale : codeLocales) {
        localeInfo.addMatchedLocale(locale);
      }
    }
    return localeInfo;
  }
  
}
