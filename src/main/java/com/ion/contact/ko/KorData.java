package com.ion.contact.ko;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

import com.ion.contact.ContactScanner;
import com.ion.contact.util.Utils;

public class KorData {

  private static char[] givenNameCharacters;
  private static char[] singleCharFamilyNames;
  private static String[] doubleCharFamilyNames;
  private static String[] orgTypes;
  private static String[] orgNames;
  private static String[] orgTitles;
  private static String[] relations;
  private static String[] titleSuffixes;
  private static String[] orgDepartments;
  private static String[] orgDepartmentSuffixes;
    
  static {
    try {
      orgTitles = Utils.loadSortedWordList(KorData.class.getResourceAsStream("org_titles.txt"), 2, 8);
      orgTypes = Utils.loadSortedWordList(KorData.class.getResourceAsStream("org_types.txt"), 2, 8);
      orgNames = Utils.loadSortedWordList(KorData.class.getResourceAsStream("org_names.txt"), 2, 32);
      orgDepartments = Utils.loadSortedWordList(KorData.class.getResourceAsStream("org_departments.txt"), 2, 32);
      orgDepartmentSuffixes = Utils.loadSortedWordList(KorData.class.getResourceAsStream("org_department_suffixes.txt"), 1, 3);
      relations = Utils.loadSortedWordList(KorData.class.getResourceAsStream("relations.txt"), 1, 4);
      doubleCharFamilyNames = Utils.loadSortedWordList(KorData.class.getResourceAsStream("double_char_family_names.txt"), 2, 2);
      singleCharFamilyNames = Utils.loadSortedCharArray(KorData.class.getResourceAsStream("single_char_family_names.txt"));
      givenNameCharacters = Utils.loadSortedCharArray(KorData.class.getResourceAsStream("name_chars.txt"));

      titleSuffixes = new String[] { "님", "비서" };
    }
    catch (Exception e) {
      e.printStackTrace();
      System.exit(-1);
    }
  }

  private static int skipOrgType(String name, int offset) {
    for (String token : orgTypes) {
      if (name.startsWith(token)) {
        return offset + token.length();
      }
    }
    return offset;
  }
  
  public static int getOrgNameMaxLength(CharSequence name_0) {
    String name = removeWhiteSpaces(name_0);
    
    int orgNameStart = skipOrgType(name, 0);
    name = name.substring(orgNameStart);

    String orgName = Utils.binarySearchPrefix(orgNames, name);
    if (orgName != null) {
      int name_len = skipOrgType(name, orgName.length());
      int text_len = getNormalizedLength(name_0, orgNameStart + name_len);
      return text_len;
    }
    return -1;
  }  

  private static int getNormalizedLength(CharSequence text, int len_without_whitespace) {
    int idx = 0;
    while (len_without_whitespace > 0) {
       if (text.charAt(idx ++) > ' ') {
         len_without_whitespace --;
       }
    }
    return idx;
  }

  private static String removeWhiteSpaces(CharSequence text) {
    StringBuilder sb = null;
    for (int i = 0; i < text.length(); i++) {
      char ch = text.charAt(i);
      if (ch <= ' ') {
        if (sb == null) {
          sb = new StringBuilder();
          sb.append(text, 0, i);
        }
      }
      else if (sb != null) {
        sb.append(ch);
      }
    }
    return sb != null ? sb.toString() : text.toString();
  }

  public static boolean isValidFamilyName(String familyName) {
    switch (familyName.length()) {
    case 1:
      return Arrays.binarySearch(singleCharFamilyNames, familyName.charAt(0)) >= 0;
    case 2:
      return Arrays.binarySearch(doubleCharFamilyNames, familyName) >= 0;
    default:
      return false;
    }
  }

  public static int getFamilyNameMaxLength(String name) {
    name = name.trim();
    switch (name.length()) {
    case 0:
      break;
    default:
      if (Arrays.binarySearch(doubleCharFamilyNames, name) >= 0) {
        return 2;
      }
    case 1:
      if (Arrays.binarySearch(singleCharFamilyNames, name.charAt(0)) >= 0) {
        return 1;
      }
    }
    return 0;
  }

  public static int getOrgTitleMaxLength(String text) {
    String title = Utils.binarySearchPrefix(orgTitles, text);
    if (title != null) {
      int len = title.length();
      if (text.length() > len) {
        String suffix = Utils.binarySearchPrefix(titleSuffixes, text.substring(len));
        if (suffix != null) {
          len += suffix.length();
        }
      }
      return len;
    }
    return 0;
  }
  
  public static int getOrgDepartmentMaxLength(String text) {
    String title = Utils.binarySearchPrefix(orgDepartments, text);
    if (title != null) {
      int len = title.length();
      if (text.length() > len) {
        String suffix = Utils.binarySearchPrefix(orgDepartmentSuffixes, text.substring(len));
        if (suffix != null) {
          len += suffix.length();
        }
      }
      return len;
    }
    return 0;
  }
  
  public static int getRelationMaxLength(String text) {
    String relation = Utils.binarySearchPrefix(relations, text);
    if (relation != null) {
      int len = relation.length();
      if (text.length() > len) {
        String suffix = Utils.binarySearchPrefix(titleSuffixes, text.substring(len));
        if (suffix != null) {
          len += suffix.length();
        }
      }
      return len;
    }
    return 0;
  }

  public static boolean isValidRelationWord(String text) {
    int idx = Arrays.binarySearch(relations, text);
    return idx >= 0;
  }


  
  public static boolean startsWithValidFullName(String name) {
    int familyNameLength = getFamilyNameMaxLength(name);

    String gn = Utils.cutWord(name, familyNameLength);
    switch (gn.length()) {
    default:
      if (!isValidGivenNameChar(gn.charAt(1))) {
        return false;
      }
      // else no-break;
    case 1:
      return isValidGivenNameChar(gn.charAt(0));
    case 0:
      return false;
    }
  }



  public static boolean isValidGivenNameChar(char ch) {
    /*
     * 이름에 쓰이는 글자인가를 판별한다.
     */
    int idx = Arrays.binarySearch(givenNameCharacters, ch);
    /*
     * TDOO: 이름에 쓰이는 문자를 모두 등록.
     */
    return true || idx >= 0;
  }

  public static ArrayList<ContactScanner> getScanners() {
    // TODO Auto-generated method stub
    return null;
  }


}
