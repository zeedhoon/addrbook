package com.ion.contact.ko;

import java.util.ArrayList;

import org.json.JSONObject;

import com.ion.contact.ContactField;
import com.ion.contact.ContactScanner;
import com.ion.contact.ContactToken;
import com.ion.contact.ScanResult;
import com.ion.contact.util.Utils;

public class NameScanner implements ContactScanner {

  @Override
  public void scan(JSONObject addrData, ScanResult scanResult) {
    Parser parser = new Parser(addrData);
    TokenList topNodes = parser.parse();
    postProcess(scanResult, topNodes);
  }
  
  public void postProcess(ScanResult suggestions, TokenList topNodes) {
    for (TokenNode root : topNodes) {
      root.registerTokens(suggestions);
      root.dumpTree(0);
    }
  }

  

  static class Parser {

    private JSONObject addrData;
    private String parsingText;
    private TokenList tokenBuckets[];
    
    public Parser(JSONObject addrData) {
      this.addrData = addrData;
    }
  
    public int init() {
      String familyName = addrData.optString(ContactField.FAMILY_NAME.getKey()).trim();
      String givenName = addrData.optString(ContactField.GIVEN_NAME.getKey()).trim();
      
      this.parsingText = (familyName + ' ' + givenName).trim();
      this.tokenBuckets = new TokenList[parsingText.length()];
      
      int fn_len = familyName.length();
      if (KorData.isValidFamilyName(familyName)) {
        /*
         * familyName이 명확히 지정된 경우
         */
        addToken(ContactField.FAMILY_NAME, 0, fn_len);
        return fn_len;
      }
      return 0;
    }

    public TokenList parse() {
      int fn_len = init();
      tokenize(fn_len);
      detectNameTokens();
      buildTokenTree(0);
      return tokenBuckets[0];
    }
  
    private TokenNode addToken(ContactField field, int pos, int len) {
      TokenList tokens = tokenBuckets[pos];
      if (tokens == null) {
        tokens = new TokenList();
        tokenBuckets[pos] = tokens;
      }
      int flags = 0;
      if (pos == 0 || 
          !Character.isLetter(parsingText.charAt(pos - 1))) {
        flags |= TokenNode.WORD_BREAK_BEFORE;
      }
      int end = pos + len;
      if (end == parsingText.length() || 
          !Character.isLetter(parsingText.charAt(end))) {
        flags |= TokenNode.WORD_BREAK_AFTER;
      }
      String tokenText = parsingText.substring(pos, pos + len);
      TokenNode newToken = new TokenNode(field, tokenText, flags);
      tokens.add(newToken);
      return newToken;
    }
    
    private void tokenize(int firstNameLength) {
      int parsingPos = firstNameLength;
      boolean skip_family_name = parsingPos > 0;
      boolean isWordStart = true;
      
      for (; parsingPos < parsingText.length(); parsingPos ++) {
        int curr_ch = parsingText.charAt(parsingPos); 
        if (curr_ch <= ' ') {
          addToken(ContactField.EX_SEPARATOR, parsingPos, 1);
          isWordStart = true;
          continue;
        }
        
        String text = parsingText.substring(parsingPos);
        int len;
        if (isWordStart) {
          if (!skip_family_name) {
            len = KorData.getFamilyNameMaxLength(text);
            if (len > 0) {
              addToken(ContactField.FAMILY_NAME, parsingPos, len);
            }
          }
          
          len = KorData.getOrgNameMaxLength(text);
          if (len > 0) {
            addToken(ContactField.ORG_NAME, parsingPos, len);
          }
        
          len = KorData.getOrgDepartmentMaxLength(text);
          if (len > 0) {
            addToken(ContactField.ORG_DEPARTMENT, parsingPos, len);
          }
          
        }
        
        len = KorData.getOrgTitleMaxLength(text);
        if (len > 0) {
          addToken(ContactField.ORG_TITLE, parsingPos, len);
        }
        
        if (Character.isLetter(curr_ch)) {
          isWordStart = false;
        }
      }
      
      String lastWord = Utils.cutLastWord(parsingText);
      if (KorData.isValidRelationWord(lastWord)) {
        int len = lastWord.length();
        addToken(ContactField.RELATION, parsingText.length() - len, len);
      }
    }
    
    public void detectNameTokens() {
      for (int offset = 0; offset < tokenBuckets.length; offset ++) {
        TokenList tokens = tokenBuckets[offset];
        if (tokens == null) {
          if (parsingText.charAt(offset) <= ' ') {
            continue;
          }
          
          int name_start = offset;
          for (; ++offset < tokenBuckets.length;) {
            if (tokenBuckets[offset] != null || parsingText.charAt(offset) <= ' ') {
              break;
            }
          }
          addToken(ContactField.GIVEN_NAME, name_start, offset-name_start);
          offset--;
        }
      }
    }
    
    public TokenList buildTokenTree(int offset) {
      TokenList tokens;
      while (true) {
        if (offset >= tokenBuckets.length) {
          return null;
        }
        tokens = tokenBuckets[offset];
        if (tokens != null) {
          break;
        }
      }

      for (TokenNode node : tokens) {
        node.children = buildTokenTree(offset + node.length());
      }
      
      return tokens;
    }
    
  }

  private static class TokenList extends ArrayList<TokenNode> {
    public void registerTokens(ScanResult suggestions) {
      for (TokenNode child : this) {
        child.registerTokens(suggestions);
      }
    }

    public void dumpTree(int indent) {
      for (TokenNode child : this) {
        child.dumpTree(indent);
      }
    }
  }
  
  private static class TokenNode extends ContactToken {
    TokenList children;
    
    public TokenNode(ContactField field, String token, int flags) {
      super(token, field, flags);
    }

    public void registerTokens(ScanResult suggestions) {
      suggestions.add(this.getField(), this.getValue(), 0);
      if (children != null) {
        children.registerTokens(suggestions);
      }
    }

    public void dumpTree(int indent) {
      for (int i = indent; --i >= 0; ) {
        System.out.print('\t');
      }
      System.out.print(getKey());
      System.out.print(':');
      System.out.println(getValue());
      if (children != null) {
        children.dumpTree(indent+1);
      }
      
    }

    public final int length() {
      return getValue().length();
    }
  }
  
  
}
