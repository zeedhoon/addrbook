package com.ion.contact.ko;

import java.util.ArrayList;

import com.ion.contact.ContactDataInspector;
import com.ion.contact.ContactScanner;

public class ContactDataInspectorImpl extends ContactDataInspector {

  public ContactDataInspectorImpl() {
    super(getScanners());
    // TODO Auto-generated constructor stub
  }

  private static ArrayList<ContactScanner> getScanners() {
    ArrayList<ContactScanner> scanners = new ArrayList<>();
    scanners.add(new NameScanner());
    // ...
    return scanners;
  }

}
