package com.ion.contact.ko;

import java.util.TreeMap;

import org.json.JSONObject;

import com.ion.contact.ContactField;
import com.ion.contact.ContactScanner;
import com.ion.contact.ContactToken;
import com.ion.contact.ScanResult;
import com.ion.contact.util.Utils;

public class OldNameScanner implements ContactScanner {

  @Override
  public void scan(JSONObject addrData, ScanResult scanResult) {
    Parser parser = new Parser(addrData);
    parser.parse();

    for (ContactToken token : parser.tokens.values()) {
      scanResult.add(token.getField(), token.getValue(), 0);
    }
  }

  static class Parser {
    private JSONObject addrData;
    private TreeMap<ContactField, ContactToken> tokens;
    private String parsingText;
    
    public Parser(JSONObject addrData) {
      this.addrData = addrData;
      this.tokens = new TreeMap<>();
    }
  
    public void parse() {
      Tokenizer p = Start.parse();
      while (p != null && parsingText.length() > 0) {
        p = p.parse();
      }
    }
  
    private void cutHead(int len, ContactField type) {
      String token = parsingText.substring(0, len).trim();
      addToken(token, type);
      parsingText = parsingText.substring(len).trim();
    }
  
    private void cutTail(String token, ContactField type) {
      addToken(token, type);
      parsingText = parsingText.substring(parsingText.length() - token.length()).trim();
    }
  
    private void addToken(String token, ContactField type) {
      tokens.put(type, new ContactToken(token, type, 0));
    }
    
    private abstract class Tokenizer {
      abstract Tokenizer parse();
    }
    
    private Tokenizer Start = new Tokenizer () {
      Tokenizer parse() {
        String familyName = addrData.optString(ContactField.FAMILY_NAME.getKey()).trim();
        String givenName = addrData.optString(ContactField.GIVEN_NAME.getKey()).trim();
        
        if (KorData.isValidFamilyName(familyName)) {
          /*
           * familyName이 명확히 지정된 경우, 항상 givenName 에 유효한 이름이
           * 저장된 것으로 가정한다. 
           */
          parsingText = givenName;
          addToken(familyName, ContactField.FAMILY_NAME);
          return GivenNameAnd__;
        }
        else {
          /*
           * familyName이 명확히 지정되지 않은 경우, familyName과 givenName 을 
           * 병합한 후 다시 분리하는 작업을 수행한다. 
           * 
           */
          if (familyName.length() > 0) {
            parsingText = familyName + ' ' + givenName;
          }
          else {
            parsingText = givenName;
          }
          return AnyNameOr__;
        }
      }
    };
  
    private Tokenizer AnyNameOr__ = new Tokenizer () {
      Tokenizer parse() {
        /*
         * 사람이름 또는 회사명을 분리한다.
         */
        int len = KorData.getOrgNameMaxLength(parsingText);
        if (len > 0) {
          /*
           * 등록된 상호명인 경우, 상호명을 분리한다.
           */
          cutHead(len, ContactField.ORG_NAME);
        }
        /*
         * 사람 이름 추출 단계로 전환.
         */
        return PersonNameOr__;
      }
    };
    
    private Tokenizer PersonNameOr__  = new Tokenizer () {
      Tokenizer parse() {
        int len = KorData.getFamilyNameMaxLength(parsingText);
        if (len > 0) {
          /*
           * familyName을 분리한다. 
           */
          cutHead(len, ContactField.FAMILY_NAME);
          /*
           * familyName 다음에는 givenName 또는 직책이 뒤따라 올 수 있다.
           */
          return OrgTitleOrGivenName__;
        }
        /*
         * givenName 추출 단계로 전환.
         */
        return GivenNameOr__;
      }
    };
  
    private Tokenizer OrgTitleOrGivenName__ = new Tokenizer () {
      Tokenizer parse() {
        int len = KorData.getOrgTitleMaxLength(parsingText);
        if (len > 0) {
          /*
           * 직책명을 추출한다. 
           */
          cutHead(len, ContactField.ORG_TITLE);
          return Extra__;
        }
        else {
          return GivenNameAnd__;
        }
      }
    };
    
    private Tokenizer GivenNameAnd__ = new Tokenizer () {
      Tokenizer parse() {
        String name = Utils.cutWord(parsingText, 0);
        if (name.length() <= 2) {
          cutHead(name.length(), ContactField.GIVEN_NAME);
          return Extra__;
        }
        return GivenNameOr__;
      }
    };
  
    private Tokenizer GivenNameOr__ = new Tokenizer () {
      Tokenizer parse() {
        if (KorData.isValidGivenNameChar(parsingText.charAt(0))) {
          int name_len = 1;
          if (KorData.isValidGivenNameChar(parsingText.charAt(1))) {
            if (KorData.getOrgTitleMaxLength(parsingText.substring(1)) <= 0 ||
                KorData.getOrgTitleMaxLength(parsingText.substring(2)) > 0) {
              name_len = 2;
            }
          }
          cutHead(name_len, ContactField.GIVEN_NAME);
        }
        return Extra__;
      }
    };
  
    private Tokenizer Extra__ = new Tokenizer () {
      Tokenizer parse() {
        /*
         * 사람이름 또는 회사명을 추출한 후 나머지 내용에 대한 처리를 한다.
         */
        StringBuilder sb = new StringBuilder();
  
        while (parsingText.length() > 0) {
          boolean token_detected = false;
          if (!tokens.containsKey(ContactField.ORG_NAME)) {
            int org_name_len = KorData.getOrgNameMaxLength(parsingText);
            if (org_name_len > 0) {
              cutHead(org_name_len, ContactField.ORG_NAME);
              token_detected = true;
            }
          }
          
          if (!tokens.containsKey(ContactField.ORG_TITLE)) {
            int title_len = KorData.getOrgTitleMaxLength(parsingText);
            if (title_len > 0) {
              cutHead(title_len, ContactField.ORG_TITLE);
              token_detected = true;
            }
          }
              
          if (!tokens.containsKey(ContactField.ORG_TITLE)) {
            int relation_len = KorData.getRelationMaxLength(parsingText);
            if (relation_len > 0) {
              cutHead(relation_len, ContactField.ORG_TITLE);
              token_detected = true;
            }
          }
              
          if (!token_detected) {
            String skip = Utils.cutWord(parsingText, 0);
            if (sb.length() > 0) {
              sb.append(' ');
            }
            sb.append(skip);
            parsingText = parsingText.substring(skip.length()).trim();
          }
        }
          
        if (sb.length() > 0) {
          if (tokens.containsKey(ContactField.ORG_NAME) ||
              tokens.containsKey(ContactField.GIVEN_NAME)) {
            addToken(sb.toString(), ContactField.ORG_DEPARTMENT);
          }
          else {
            addToken(sb.toString(), ContactField.GIVEN_NAME);
          }
        }
        return null;
      }
    };
  }
}
