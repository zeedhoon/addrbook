package com.ion.contact.ko;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.ion.contact.ContactDataInspector;
import com.ion.contact.ContactField;
import com.ion.contact.ScanResult;
import com.ion.contact.util.Utils;

class NameCorrectionTest {
  
  @BeforeAll
  public static void setUp() {
  }

  static class TestItem extends JSONObject {
    JSONObject testInput;
    JSONObject expectedResult;
    
    TestItem(JSONObject testInput, JSONObject expectedResult) {
      this.testInput = testInput;
      this.expectedResult = expectedResult;
    }
    
    public void checkResult(ScanResult res) {
      assertTrue(Utils.equals(this.expectedResult, res.getPrimarySuggestion()));
    }
  }
  
  static TestItem testItems[];
  static {
    testItems = new TestItem[] {
        new TestItem(
            new JSONObject()
              .put(ContactField.GIVEN_NAME.getKey(), "김형수부장"),
            new JSONObject()
              .put(ContactField.FAMILY_NAME.getKey(), "김")
              .put(ContactField.GIVEN_NAME.getKey(), "형수")
              .put(ContactField.ORG_TITLE.getKey(), "부장")
        ),
        new TestItem(
            new JSONObject()
              .put(ContactField.FAMILY_NAME.getKey(), "김형수부장"),
            new JSONObject()
              .put(ContactField.FAMILY_NAME.getKey(), "김")
              .put(ContactField.GIVEN_NAME.getKey(), "형수")
              .put(ContactField.ORG_TITLE.getKey(), "부장")
        ),
        new TestItem(
            new JSONObject()
              .put(ContactField.FAMILY_NAME.getKey(), "김부장"),
            new JSONObject()
              .put(ContactField.FAMILY_NAME.getKey(), "김")
              .put(ContactField.ORG_TITLE.getKey(), "부장")
        ),
    };
  }
  
  @Test
  void testName() {
    for (TestItem test : testItems) {
      ScanResult res = ContactDataInspector.inspect(test.testInput, Locale.KOREAN);
      test.checkResult(res);
    }
  }

  @Test
  void testSampleFile() throws IOException {
    InputStream in = this.getClass().getResourceAsStream("test_sample-ko.json");
    String s = Utils.toString(in);
    JSONArray dataArray = new JSONArray(s);
    for (int i = 0; i < dataArray.length(); i ++) {
      JSONObject json = dataArray.getJSONObject(i);
      System.out.print(json.toString(2));
      System.out.println(" --> ");
      ScanResult res = ContactDataInspector.inspect(json, Locale.KOREAN);
      System.out.println("=========================================\n");
    }
  }
}
